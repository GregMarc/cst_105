import java.util.Scanner;

/**
 * 
 * @author gregmarc
 *This is my own work
 *Programming Exercise 02, CST 105
 *January 22, 2017
 */
public class PE2 {

	public static void main(String[] args) {
		
		Scanner system = new Scanner (System.in);//initialize the scanner for input from the system
		double inputDouble; //initialize the input as a double
		
		System.out.println("Exercise 2.4");
		System.out.print("Enter a number in pounds: ");//prompt for a value of pounds
		inputDouble = system.nextDouble();//store the double input into the variable inputDouble
		double a = inputDouble * 0.454;//calculate the pounds to kilograms
		System.out.println(inputDouble + " pounds is " + a + " kilograms");//display the values
		
		
		System.out.println();
		System.out.println("Exercise 2.5");
		double aa, bb;//initialize the variables needed as doubles
		System.out.print("Enter the subtotal and a gratuity rate: ");//prompt for the subtotal and grauity
		aa = system.nextDouble();//store the subtotal
		bb = system.nextDouble();//store the gratuity
		double gratuity = (aa * bb) * .01;//calculate the gratuity
		double total = aa + gratuity;//calculate the total
		System.out.println("The gratuity is $" + gratuity + " and total is $" + total);//display the gratuity and total
		
		
		System.out.println();
		System.out.println("Exercise 2.20");
		System.out.print("Enter balance and interest rate (e.g., 3 for 3%): ");//prompt for the balance and interest rate
		double balance = system.nextDouble();//store the balance
		double interestRate = system.nextDouble();//store the interest rate
		double interest = balance * (interestRate / 1200);//calculate the interest
		System.out.println("The interest is " + interest);//display the interest
		

		System.out.println();
		System.out.println("Exercise 2.23");
		System.out.print("Enter investment amount: ");//prompt for the investment amount
		double investment = system.nextDouble();//store the investment amount
		System.out.print("Enter annual interest rate in percentage: ");//prompt for the annual interest rate
		double annualInterestRate = system.nextDouble();//store the annual interest rate
		System.out.print("Enter number of years: ");//prompt for the years
		double years = system.nextDouble();//store the years
		double investmentValue = investment * Math.pow((1 + annualInterestRate / 1200), (years * 12));//calcualte the investment total
		System.out.print("Accumulated value is: $" + investmentValue);//display the investment total

	}

}
