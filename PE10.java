/*
 *@author gregmarc
 *This is my own work
 *Programming Exercise 10 CST 105
 *April 10, 2017
 */
public class Practice01 {

	public static void main(String[] args) {
		
		Person me = new Person ("Greg", " 5621...", "gregorymarc@gcu.edu", "6235375681");
		Student greg = new Student ("Greg Student", "5621 W. ....", "gmarc@gcu.edu", "6234550246", "Senior");
		System.out.print(me.toString());
		System.out.print(greg.toString());
		
	}

}
class Staff extends Employee{
	protected String title;
	
	public Staff (String name, String address, String email, String phoneNum, String office, double salary, String dateHired, String title){
		super(name, address, email, phoneNum, office, salary, dateHired);
		this.title = title;
	}
	
	public String tostring(){
		String str = ("\n\t\t - Class: Staff - \n" + "name: " + name + "\taddress: " + address + "\temail: " + email + "\tphoneNum: " + phoneNum + "\toffice: " + office + "\tsalary: " + salary + "\tdateHired: " + dateHired + "\ttitle: " + title);
		return str;
	}
}

class Faculty extends Employee{
	protected String officeHours, rank;
	
	public Faculty (String name, String address, String email, String phoneNum, String office, double salary, String dateHired, String officeHours, String rank){
		super (name, address, email, phoneNum, office, salary, dateHired);
		this.officeHours = officeHours;
		this.rank = rank;
	}
	
	public String tostring(){
		String str = ("\n\t\t - Class: Faculty - \n" + "name: " + name + "\taddress: " + address + "\temail: "  + email + "\tphoneNum: " + phoneNum + "\toffice: " + office + "\tsalary: " + salary + "\tdateHired: " + dateHired + "\tofficeHours: " + officeHours + "\trank:" + rank);
		return str;
	}
}

class Employee extends Person{
	protected String office, dateHired;
	protected double salary;
	
	public Employee (String name, String address, String email, String phoneNum, String office, double salary, String dateHired){
		super(name, address, email, phoneNum);
		this.office = office;
		this.salary = salary;
		this.dateHired = dateHired;
	}
	
	public String toString(){
		String str = ("\n\t\t - Class: Employee - \n" + "name: " + name + "\taddress: " + address + "\temail: " + email + "\tphoneNum: " + phoneNum + "\toffice: " + office + "\tsalary: " + salary + "\tdateHired: " + dateHired);
		return str;
	}
}

class Student extends Person{
	private final String STATUS;
	
	public Student (String name, String address, String email, String phoneNum, String status){
		super(name, address, email, phoneNum);
		this.STATUS = status;
	}
	
	public String toString(){
		String str = ("\n\t\t - Class: Student - \n" + "name: " + name + "\taddress: " + address + "\temail: " + email + "\tphoneNum: " + phoneNum + "\tstatus: " + STATUS);
		return str;
	}
}


class Person{
	
	protected String name, address, email, phoneNum;
	
	public Person (String name, String address, String email, String phoneNum){
		this.name = name;
		this.address = address;
		this.email = email;
		this.phoneNum = phoneNum;
	}
	
	public String toString(){
		String str = ("\n\t\t - Class: Person - \n" + " name: " + name + "\taddress: " + address + "\temail: " + email + "\tphoneNum: " + phoneNum);
		return str;
	}
}
