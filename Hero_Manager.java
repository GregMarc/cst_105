/*
This is my own work
@gregmarc
CST 105 Milestone 
March 12, 2017
*/
public class Hero_Manager {
	
	private Hero [] Heroes = new Hero [113];
	
	private int numItems;
	
	//Class Constructor
	public Hero_Manager () {
		this.numItems = 0;
		createHeroes ();
	}
	
	public void displayHeroes () {
		for (int i = 0; i < numItems; i++){
			System.out.print(Heroes[i] + "\n");
			//System.out.print(Heroes[i].getName());
		}
	}
	
	public void createHeroes () {
		//Type
		String agil = "Agility";
		String str = "Strength";
		String in = "Intelligence";
		
		//Attack Type
		String ran = "Ranged";
		String mel = "Melee";
		
		//Roles
		String c = "Carry";
		String d = "Disabler";
		String e = "Escape";
		String du = "Durable";
		String i = "Initiator";
		String j = "Jungler";
		String n = "Nuker";
		String p = "Pusher";
		String s = "Support";
		
		//Abilities followed by Ulti
		//HP Max
		//Mana Max
		//Armor Max
		//Move Speed
		//Damage Max
		//Sight and then Attack range
		Hero Lina = new Hero ("Lina", in, ran, c, n, d, "Dragon Slave", "Light Strike Array", "Fiery Soul", "Laguna Blade", 1556, 1609, 9, 295, 155, 1800, 670);
		Heroes [numItems++] = Lina;
		
		Hero Windranger = new Hero ("Wind Ranger", in, ran, c, s, d, "Shackleshot", "Powershot", "Windrun", "Focus Fire", 1955, 1357, 9, 295, 138, 1800, 600);
		Heroes [numItems++] = Windranger;
		
		Hero AntiMage = new Hero ("Anti-Mage", agil, mel, c, e, n, "Mana Break", "Blink", "Spell Shield", "Mana Void", 1495, 1017, 14, 315, 140, 800, 150);
		Heroes [numItems++] = AntiMage;
		
		Hero Sniper = new Hero ("Sniper", agil, ran, c, n, "", "Shrapnel", "Headshot", "Take Aim", "Assassinate", 1609, 1266, 13, 290, 122, 1800, 550);
		Heroes [numItems++] = Sniper;
		
		Hero TreantProtector = new Hero ("Treant Protector", str, mel, s, i, e, "Eyes In The Forest", "Leech Seed", "Living Armor", "Overgrowth", 2510, 1043, 11, 295, 194, 800, 150);
		Heroes [numItems++] = TreantProtector;
		
		Hero BrewMaster = new Hero ("BrewMaster", str, mel, c, d, n, "Thunder Clap", "Drunken Haze", "Drunken Brawler", "Primal Split", 2289, 832, 11, 300, 149, 800, 150);
		Heroes [numItems++] = BrewMaster;
		
		Hero Alchemist = new Hero ("Alchemist", str, mel, c, s, n, "Acid Spray", "Unstable Concoction", "Chemical Rage", "Unstable Concoction Throw", 1826, 1147, 8, 295, 121, 800, 150);
		Heroes [numItems++] = Alchemist;
		
		Hero Huskar = new Hero ("Huskar", str, ran, c, du, i, "Inner Vitality", "Burning Spear", "Berserker's Blood", "Life Break", 2023, 962, 9, 300, 129, 800, 400);
		Heroes [numItems++] = Huskar;
		
		Hero OmniKnight = new Hero ("OmniKnight", str, mel, s, du, n, "Purification", "Repel", "Degen Aura", "Guardian Angel", 2225, 1043, 14, 305, 150, 800, 150);
		Heroes [numItems++] = OmniKnight;
		
		Hero Clockwerk = new Hero ("Clockwerk", str, mel, i, d, n, "Battery Assault", "Power Cogs", "Rocket Flare", "Hookshot", 2308, 887, 12, 315, 147, 800, 150);
		Heroes [numItems++] = Clockwerk;
		
		Hero DragonKnight = new Hero ("Dragon Knight", str, mel, c, p, d, "Breathe Fire", "Dragon Tail", "Dragon Blood", "Elder Dragon Form", 2168, 985, 14, 290, 143, 800, 150);
		Heroes [numItems++] = DragonKnight;
		
		Hero BeastMaster = new Hero ("BeastMaster", str, mel, i, d, n, "Wild Axes", "Call of the Wild: Hawk", "Inner Beast", "Primal Roar", 1970, 1061, 13, 310, 141, 800, 150);
		Heroes [numItems++] = BeastMaster;
		
		Hero Kunkka = new Hero ("Kunkka", str, mel, c, d, n, "Torrent", "Tidebringer", "Ghostship", "Return", 2354, 962, 11, 300, 152, 800, 150);
		Heroes [numItems++] = Kunkka;
		
		Hero Tiny = new Hero ("Tiny", str, mel, c, n, p, "Avalanche", "Toss", "Craggy Exterior", "Grow", 2392, 980, 6, 285, 161, 800, 150);
		Heroes [numItems++] = Tiny;
		
		Hero Sven = new Hero ("Sven", str, mel, c, du, n, "Storm Hammer", "Great Cleave", "Warcry", "God's Strength", 2198, 874, 14, 290, 151, 800, 150);
		Heroes [numItems++] = Sven;
		
		Hero EarthShaker = new Hero ("Earth Shaker", str, mel, s, i, n, "Fissure", "Enchant Totem", "Aftershock", "Echo Slam", 2270, 1030, 10, 310, 146, 800, 150);
		Heroes [numItems++] = EarthShaker;
		
		Hero Drow = new Hero ("Drow", agil, ran, c, d, p, "Frost Arrows", "Gust", "Precision Aura", "Marksmanship", 1583, 892, 10, 290, 117, 1800, 625);
		Heroes [numItems++] = Drow;
		
		Hero CrystalMaiden = new Hero ("Crystal Maiden", in, ran, s, d, j, "Crystal Nova", "Frostbite", "Arcane Aura", "Freezing Field", 1609, 1373, 9, 280, 131, 1800, 600);
		Heroes [numItems++] = CrystalMaiden;
		
	}

}
