/*
 *@author gregmarc
 *This is my own work
 *Programming Exercise 07, CST 105
 *February 17, 2017
 */

import java.util.*;

public class PE7 {

	public static void main(String[] args) {
		
		
		Ch7Exercise2();
		Ch7Exercise7();
		
		
		System.out.print("\n\nProgramming Exercise 7.8\n");
		Scanner kb = new Scanner(System.in);
		double [] doubleArray = new double [10];
		
		for (int i = 0; i < 10; i++){
			System.out.print("Enter in a value: ");
			doubleArray [i] = kb.nextDouble();
		}
		
		System.out.print("\nThe average is: " + average (doubleArray));

	}

	private static void Ch7Exercise2() {
		System.out.print("Programming Exercise 7.2\n");
		
		Scanner kb = new Scanner(System.in);
		int [] intArray = new int [10];
		
		for (int i = 0; i < 10; i++){
			System.out.print("Enter in an integer value: ");
			intArray [i] = kb.nextInt();
		}
		
		for (int i = intArray.length - 1; i >= 0; i--){
			System.out.print(intArray [i] + "  ");
		}
		System.out.print("\n");
	}

	private static int average (int [] array){
		int mean = 0;//initialize the mean
		
		for (int i = 0; i < array.length; i++){//add the values of the array [] to the mean
			mean = mean + array [i];
		}
		mean = mean / array.length;//divide the value of mean by the length of the array
		return (mean);//return the mean value
	}
	
	private static double average (double [] array){
		double mean = 0.0;//initialize the mean
		
		for (int i = 0; i < array.length; i++){//add the values of the array [] to the mean
			mean = mean + array [i];
		}
		mean = mean / array.length;//divide the value of mean by the length of the array
		return (mean);//return the mean value
	}

	private static void Ch7Exercise7() {
		System.out.print("\nProgramming Exercise 7.7" );
		
		int [] randomNums = new int [100];
		
		Random r = new Random();//create random number generator
		
		for (int i = 0; i < 100; i++){ //Store the random numbers in the array
			randomNums [i] = r.nextInt(10);
		}
		
		int [] countNums = new int [10];//create an array for the numbers being counted
		
		for (int i = 0; i < 100; i++){
			countNums [randomNums [i]] ++;//use the value from randomNums [] to index the countNums [] and add 1 to it
			//countNums [randomNums [i]] = countNums [randomNums [i]] + 1;//same thing as above, just drawn out
			
			/*switch (randomNums [i]){
				case 0: countNums [0] = countNums [0] + 1; break;
				case 1: countNums [1] = countNums [1] + 1; break;
				case 2: countNums [2] = countNums [2] + 1; break;
				case 3: countNums [3] = countNums [3] + 1; break;
				case 4: countNums [4] = countNums [4] + 1; break;
				case 5: countNums [5] = countNums [5] + 1; break;
				case 6: countNums [6] = countNums [6] + 1; break;
				case 7: countNums [7] = countNums [7] + 1; break;
				case 8: countNums [8] = countNums [8] + 1; break;
				case 9: countNums [9] = countNums [9] + 1; break;
			}*/
		}
		
		System.out.print("\n" + "Number\t\t" + "Number of times that number was generated");//create the table headers
		
		for (int i = 0; i < 10; i ++){//use a loop to print out the number and the value of the times it was generated
			System.out.print("\n" + i + "\t\t" + countNums [i]);
		}
		
	}

}
