/**
 * 
 * @author gregmarc
 *This is my own work
 *January 13, 2017
 *Programming Exercises 01,  CST105
 */
//import java.*;
public class PE1 {

	public static void main(String[] args) {
		
		System.out.println("Exercise 1.4"); //begin Exercise 1.4
		
		int [] aa = {1,2,3,4}; //creates a list with 4 spaces {1,2,3,4}
		int [] bb = new int [4]; //{null, null, null, null}
		int [] cc = new int [4]; //{null, null, null, null}
		
		int i = 0;
		
		while (i < 4){ //only because I LOVE using while loops
			bb[i] = aa[i] * aa[i];
			cc[i] = aa[i] * aa[i] * aa[i];
			i++;
		}
		/*for (int i = 0; i < 4; i++){ //for loops instead of while
			b[i] = a[i] * a[i];
			c[i] = a[i] * a[i] * a[i];
		}*/
		
		System.out.println( "a" + "\t" + "a^2" + "\t" + "a^3"); //print out the headers
		
		i = 0;
		while (i < 4){ //LOVE using while loops
			System.out.println(aa[i] + "\t" + bb[i] + "\t" + cc[i]);
			i++;
		}
		
		/*for (int i = 0; i < 4; i++){ //For loops instead of while
			System.out.println(a[i] + "\t" + b[i] + "\t" + c[i]);
		}*/
		
		System.out.println();
		System.out.println("Exercise 1.7"); //begin Exercise 1.7
		
		double pi1, pi2; //initialize the pi variables
		
		pi1 = 4.0 * ((1.0) - (1.0/3) + (1.0/5) - (1.0/7) + (1.0/9) - (1.0/11));
		pi2 = 4.0 * ((1.0) - (1.0/3) + (1.0/5) - (1.0/7) + (1.0/9) - (1.0/11) + (1.0/13));
		
		
		System.out.println(pi1); //print out pi #1
		System.out.println(pi2); //print out pi #2
		
		System.out.println();
		System.out.println("Exercise 1.8"); //begin Exercise 1.8
		
		double perimeter, area;
		double radius = 5.5;
		
		perimeter = 2 * radius * Math.PI; //calculate the perimeter
		area = radius * radius * Math.PI; //calculate the area
		
		System.out.println("Perimeter = " + perimeter); //print out the perimeter
		System.out.println("Area = " + area); //print out the area
		
		System.out.println();
		System.out.println("Exercise 1.10"); //begin Exercise 1.10
		
		double averageTime = 45.5 / 60.0; //get the average time
		double distanceMiles = 14.0 / 1.6; //convert from km to miles
		
		double average = distanceMiles / averageTime; //calculate the average
		
		System.out.println("Average: " + average); //print out the average
		
		//System.out.printf("Average: %.2f%n", 10); //print it to 2 decimal places
		
		//System.out.printf("Average: %.2f%n", average); //print it to 2 decimal places
		
		System.out.println();
		System.out.println("Exercise 1.13"); //begin Exercise 1.13
		
		double a, b, c, d, e, f, x, y, ed, bf, ad, bc, af, ec; //initialize variables
		
		a = 3.4; b = 50.2; e = 44.5; //store the initial equation
		c = 2.1; d = 0.55; f = 5.9; //store the initial equation
		
		ed = (e * d); //calculate the value for ed
		bf = (b * f); //calculate the value for bf
		ad = (a * d); //calculate the value for ad
		bc = (b * c); //calculate the value for bc
		
		x = ((ed - bf) / (ad - bc)); //calculate the value for x
		
		af = (a * f); //calculate the value for af
		ec = (e * c); //calculate the value for ec
		
		y = ((af - ec) / (ad - bc)); //calculate the value for y
		
		System.out.println("x = " + x); //print out the value for x
		System.out.println("y = " + y); //print out the value for y

	}

}
