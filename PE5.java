import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class PE5 {

	public static void main(String[] args) {
		
		Ch5Exercise3();
		Ch5Exercise18A();
		Ch5Exercise18B();
		Ch5Exercise18C();
		Ch5Exercise18D();
		Ch5Exercise25();
		//timeConversion1();
		//timeConversion2();

	}

	private static void timeConversion2() {
		System.out.print("\n");
		
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter in a time: ");
		double input = kb.nextDouble();
		
		String split = Double.toString(input);
		String[] splitArr = split.split("\\.");
		
		double a, b;
		a = Double.parseDouble(splitArr [0]);
		b = Double.parseDouble(splitArr [1]);
		
		System.out.printf("The time enter is: %.0f", a); System.out.printf(":%.0f", (b * .1 * 60));
		
	}

	private static void timeConversion1() {
		System.out.print("\n\n");
		
		Scanner kb = new Scanner(System.in);
		System.out.print("Enter in a 24-hour time: ");
		String time = kb.nextLine();
		
		try {
	           SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
	           SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
	           Date _24HourDt = _24HourSDF.parse(time);
	           System.out.println(_12HourSDF.format(_24HourDt));
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
	}

	private static void Ch5Exercise3(){
		System.out.print("Ch 5 Exercise 3\n");
		System.out.print("Kilograms \t Pounds");//create the table headers
		for (int k = 0; k < 200; k++){//loop to calculate the values and print them out
			double lbs = k * 2.2;
			System.out.print("\n" + k + "\t\t");
			System.out.printf("%.1f", lbs);
		}
		
		System.out.print("\n\n");
	}
	
	private static void Ch5Exercise18A(){
		System.out.print("Ch 5 Exercise 18 A\n");
		for (int i = 1; i <= 6; i++){
			for (int j = 1; j <= i; j++){
				System.out.print(j + " ");
			}
			System.out.print("\n");
		}
		
		System.out.print("\n");
	}
	
	private static void Ch5Exercise18B(){
		System.out.print("Ch 5 Exercise 18 B\n");
		for (int i = 6; i >= 1; i--){
			for (int j = 1; j <= i; j++){
				System.out.print(j + " ");
			}
			
			System.out.print("\n");
		}
		
		System.out.print("\n");
	}
	
	private static void Ch5Exercise18C(){
		System.out.print("Ch 5 Exercise 18 C\n");
		for (int i = 1; i <= 6; i++){
			for (int k = 5; k >= i; k--){
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++){
				System.out.print(j + " ");
			}
			
			System.out.print("\n");
		}
		
		System.out.print("\n");
	}
	
	private static void Ch5Exercise18D(){
		System.out.print("Ch 5 Exercise 18 D\n");
		for (int i = 6; i >= 1; i--){
			for (int k = 6; k > i; k--){
				System.out.print("  ");
			}
			for (int j = 1; j <= i; j++){
				System.out.print(j + " ");
			}
			
			System.out.print("\n");
		}
		
		System.out.print("\n");
	}
	
	private static void Ch5Exercise25(){
		System.out.print("Ch 5 Exercise 25");
		
		double sum = 0;
		
		for (int k = 10000; k <= 100000; k = k + 10000){
			sum = 0;
			for (int i = 1; i <= k; i++){
				sum += Math.pow(-1, i + 1) / (2 * i - 1);
			}
			sum = sum * 4.0;
			System.out.print("\nThe value of pi at a precision of " + k + " is " + sum);
		}
	}
	
}
