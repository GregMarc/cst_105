/*
 *@author gregmarc
 *This is my own work
 *MileStone 02, Hero class, CST 105
 *March 1, 2017
 */

public class Hero {
	
	//Class constructor 
	private String name;
	private String type;
	private String attackType;
	private String role1;
	private String role2;
	private String role3;
	private String ability1;
	private String ability2;
	private String ability3;
	private int HPMax;
	private int manaMax;
	private int armorMax;
	private int moveSpeed;
	private int damageMax;
	private double sightRange;
	private double attackRange;
	
	public Hero (){
		name = "";
		type = "";
		attackType = "";
		role1 = "";
		role2 = "";
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name){
		this.name = name;
		type = "";
		attackType = "";
		role1 = "";
		role2 = "";
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type){
		this.name = name;
		this.type = type;
		attackType = "";
		role1 = "";
		role2 = "";
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		role1 = "";
		role2 = "";
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		role2 = "";
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		role3 = "";
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		ability1 = "";
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		ability2 = "";
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		ability3 = "";
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		HPMax = 0;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		manaMax = 0;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		armorMax = 0;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax, int armorMax){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		this.armorMax = armorMax;
		moveSpeed = 0;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax, int armorMax, int moveSpeed){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		this.armorMax = armorMax;
		this.moveSpeed = moveSpeed;
		damageMax = 0;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax, int armorMax, int moveSpeed, int damageMax){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		this.armorMax = armorMax;
		this.moveSpeed = moveSpeed;
		this.damageMax = damageMax;
		sightRange = 0.0;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax, int armorMax, int moveSpeed, int damageMax, double sightRange){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		this.armorMax = armorMax;
		this.moveSpeed = moveSpeed;
		this.damageMax = damageMax;
		this.sightRange = sightRange;
		attackRange = 0.0;
	}
	
	public Hero (String name, String type, String attackType, String role1, String role2, String role3, String ability1, String ability2, String ability3, int HPMax, int manaMax, int armorMax, int moveSpeed, int damageMax, double sightRange, double attackRange){
		this.name = name;
		this.type = type;
		this.attackType = attackType;
		this.role1 = role1;
		this.role2 = role2;
		this.role3 = role3;
		this.ability1 = ability1;
		this.ability2 = ability2;
		this.ability3 = ability3;
		this.HPMax = HPMax;
		this.manaMax = manaMax;
		this.armorMax = armorMax;
		this.moveSpeed = moveSpeed;
		this.damageMax = damageMax;
		this.sightRange = sightRange;
		this.attackRange = attackRange;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAttackType() {
		return attackType;
	}

	public void setAttackType(String attackType) {
		this.attackType = attackType;
	}

	public String getRole1() {
		return role1;
	}

	public void setRole1(String role1) {
		this.role1 = role1;
	}

	public String getRole2() {
		return role2;
	}

	public void setRole2(String role2) {
		this.role2 = role2;
	}

	public String getRole3() {
		return role3;
	}

	public void setRole3(String role3) {
		this.role3 = role3;
	}

	public String getAbility1() {
		return ability1;
	}

	public void setAbility1(String ability1) {
		this.ability1 = ability1;
	}

	public String getAbility2() {
		return ability2;
	}

	public void setAbility2(String ability2) {
		this.ability2 = ability2;
	}

	public String getAbility3() {
		return ability3;
	}

	public void setAbility3(String ability3) {
		this.ability3 = ability3;
	}

	public int getHPMax() {
		return HPMax;
	}

	public void setHPMax(int hPMax) {
		HPMax = hPMax;
	}

	public int getManaMax() {
		return manaMax;
	}

	public void setManaMax(int manaMax) {
		this.manaMax = manaMax;
	}

	public int getArmorMax() {
		return armorMax;
	}

	public void setArmorMax(int armorMax) {
		this.armorMax = armorMax;
	}

	public int getMoveSpeed() {
		return moveSpeed;
	}

	public void setMoveSpeed(int moveSpeed) {
		this.moveSpeed = moveSpeed;
	}

	public int getDamageMax() {
		return damageMax;
	}

	public void setDamageMax(int damageMax) {
		this.damageMax = damageMax;
	}

	public double getSightRange() {
		return sightRange;
	}

	public void setSightRange(double sightRange) {
		this.sightRange = sightRange;
	}

	public double getAttackRange() {
		return attackRange;
	}

	public void setAttackRange(double attackRange) {
		this.attackRange = attackRange;
	}
	
	
}
