/**
 * 
 *@author gregmarc
 *This is my own work
 *Programming Exercise 02, CST 105
 *January 22, 2017
 */
import java.util.*;


public class PE3 {

	public static void main(String[] args) {
		
		Ch3Exercise2();
		Ch3Exercise4();
		Ch3Exercise20();
		Ch3Exercise26();
		
	}

	public static void Ch3Exercise2() {
		System.out.println("Exercise 3.2 ");
		int number1 = (int)(System.currentTimeMillis() % 10);
		int number2 = (int)(System.currentTimeMillis() / 7 % 10);
		int number3 = (int)(System.currentTimeMillis() / 3 % 10);
		Scanner system = new Scanner (System.in);
		
		System.out.print("What is " + number1 + " + " + number2 +  " + " + number3 +  "? ");
		int answer = system.nextInt();
		System.out.println(number1 + " + " + number2 + " + " + number3 + " = " + answer + " is " +
		(number1 + number2 + number3 == answer));
	}

	public static void Ch3Exercise4() {
		System.out.println();
		System.out.print("Exercise 3.4");
		System.out.println();
		int month = (int)(System.currentTimeMillis() % 12); //generate a pseudo random number using the computer time
		switch (month){
		case 0: System.out.println("January"); break;
		case 1: System.out.println("February"); break;
		case 2: System.out.println("March"); break;
		case 3: System.out.println("April"); break;
		case 4: System.out.println("May"); break;
		case 5: System.out.println("June"); break;
		case 6: System.out.println("July"); break;
		case 7: System.out.println("August"); break;
		case 8: System.out.println("September"); break;
		case 9: System.out.println("October"); break;
		case 10: System.out.println("November"); break;
		case 11: System.out.println("December"); break;
		}
		/*if (month == 0){
			System.out.println("January");
		}
		if (month == 1){
			System.out.println("February");
		}
		if (month == 2){
			System.out.println("March");
		}
		if (month == 3){
			System.out.println("April");
		}
		if (month == 4){
			System.out.println("May");
		}
		if (month == 5){
			System.out.println("June");
		}
		if (month == 6){
			System.out.println("July");
		}
		if (month == 7){
			System.out.println("August");
		}
		if (month == 8){
			System.out.println("September");
		}
		if (month == 9){
			System.out.println("October");
		}
		if (month == 10){
			System.out.println("November");
		}
		if (month == 11){
			System.out.println("December");
		}*/
		
		
	}
	
	public static void Ch3Exercise20() {
		System.out.println();
		System.out.println("Exercise 3.20");
		
		Scanner system = new Scanner (System.in);
		double chill, wind;
		System.out.print("Enter the temperature in Fahrenheit between -58°F and 41°F: ");
		chill = system.nextDouble();
		while ((chill < -58) || (chill > 41)){
			System.out.print("The temperature entered is invalid, enter another temperature between -58 and 41: ");
			chill = system.nextDouble();
		}
		System.out.print("Enter the wind speed (>=2) in miles per hour: ");
		wind = system.nextDouble();
		while (wind < 2){
			System.out.print("The wind enter was not valid, enter a wind speed greater than or equal to 2 mph: ");
			wind = system.nextDouble();
		}
		wind = Math.pow(wind, 0.16);
		double windChill = (35.74 + (0.6215 * chill) - (35.75 * wind) + (0.4275 * chill * wind));
		System.out.printf("The wind-chill index using the given values is: " + windChill);
		System.out.println();
	}
	
	public static void Ch3Exercise26() {
		System.out.println();
		System.out.print("Exercise 3.26");
		System.out.println();
		int value;
		Scanner system = new Scanner (System.in);
		System.out.print("Enter an integer: ");
		value = system.nextInt();
		System.out.print("Is " + value + " divisible by 5 and 6? " + (((value % 5) == 0) && ((value % 6) == 0)));
		System.out.println();
		System.out.print("Is " + value + " divisible by 5 or 6? " + (((value % 5) == 0) || ((value % 6) == 0)));
		System.out.println();
		System.out.print("Is " + value + " divisible by 5 or 6, but not both? " + !(((value % 5) == 0) && ((value % 6) == 0)));
		
	}
}
